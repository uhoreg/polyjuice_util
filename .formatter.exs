# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,lib,test}/**/*.{ex,exs}"]
]

# This file comes from the mix template, so use the copyright info for mix.
# SPDX-FileCopyrightText: The Elixir Team
# SPDX-License-Identifier: Apache-2.0
