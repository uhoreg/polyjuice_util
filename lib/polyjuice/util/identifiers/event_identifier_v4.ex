# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-FileCopyrightText: 2022 Hubert Chathi <hubert@uhoreg.ca>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.Identifiers.V4.EventIdentifier do
  @type t :: %__MODULE__{
          sigil: String.t(),
          opaque_id: String.t()
        }
  alias Polyjuice.Util.Identifiers.V4.EventIdentifier

  @enforce_keys [:sigil, :opaque_id]

  defstruct [
    :sigil,
    :opaque_id
  ]

  @sigil "$"

  @behaviour Polyjuice.Util.Identifiers

  @impl Polyjuice.Util.Identifiers
  def new!(identifier) do
    case parse(identifier) do
      {:ok, event} -> event
      {:error, error} -> raise ArgumentError, to_string(error)
    end
  end

  @impl Polyjuice.Util.Identifiers
  def new(identifier), do: parse(identifier)

  @impl Polyjuice.Util.Identifiers
  def valid?(%EventIdentifier{} = this) do
    Regex.match?(~r"^[-_a-zA-z0-9]{43}$", this.opaque_id)
  end

  @impl Polyjuice.Util.Identifiers
  def valid?(identifier_string) do
    case parse(identifier_string) do
      {:ok, _} -> true
      _ -> false
    end
  end

  def fqid(%EventIdentifier{} = this) do
    "#{@sigil}#{this.opaque_id}"
  end

  @impl Polyjuice.Util.Identifiers
  @spec parse(String.t()) :: {:ok, EventIdentifier.t()} | {:error, atom}
  def parse(id) when is_binary(id) do
    with @sigil <> opaque_id <- id,
         event_identifier = %EventIdentifier{sigil: @sigil, opaque_id: opaque_id},
         true <- valid?(event_identifier) do
      {:ok, event_identifier}
    else
      _ -> {:error, :invalid_event_id}
    end
  end
end

defimpl String.Chars, for: Polyjuice.Util.Identifiers.V4.EventIdentifier do
  def to_string(this), do: Polyjuice.Util.Identifiers.V4.EventIdentifier.fqid(this)
end

defimpl Jason.Encoder, for: Polyjuice.Util.Identifiers.V4.EventIdentifier do
  def encode(this, opts) do
    Polyjuice.Util.Identifiers.V4.EventIdentifier.fqid(this) |> Jason.Encode.string(opts)
  end
end
