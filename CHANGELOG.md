0.2.3
=====

Changes:

* Change UUID implementation from `uuid` to `uniq` (when possible -- thanks to
  Nicolas Jouanin)
  * use UUIDv7 for unique ID generation when using `uniq`
* Update Jason version
* Auth rule fixes (thanks to Ben W.)

0.2.2
=====

Features:

* Add a function for calculating the reference hash of an event.

Fixes:

* Fix compatibility with Elixir 1.13.
* Throw an error when calculating the content hash of an event that can't be
  serialized.

0.2.1
=====

Features:

* Add convenience types for Matrix events.
* Add module for using Matrix URIs.

0.2.0
=====

Breaking changes:

* The `Polyjuice.Util.Event` module has been renamed to
  `Polyjuice.Util.RoomVersion`, as it provides more functions than just
  event-related functions.

Features:

* Add error modules. (Thanks to Nico Jouanin)
* Add randomizer. (Thanks to Nico Jouanin)
* Add function for computing content hash for JSON objects. (Thanks to Nico Jouanin)
* Add modules for handling identifiers. (Thanks to Nico Jouanin)
* Add function to create a verify key using the algorithm given in the key ID.
* Add functions for checking room permissions and event authorization.

Deprecated:

* `Polyjuice.Util.JSON.verify/3` has been deprecated;
  `Polyjuice.Util.JSON.signed?/3` should be used instead

Fixes:

* Fix redaction of events with no type.

0.1.0
=====

Initial release

<!--
SPDX-FileCopyrightText: 2019-2021 Hubert Chathi <hubert@uhoreg.ca>
SPDX-License-Identifier: CC0-1.0
-->
