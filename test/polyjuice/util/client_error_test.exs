# Copyright 2021 Nicolas Jouanin <nico@beerfactory.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-FileCopyrightText: 2021 Nicolas Jouanin <nico@beerfactory.org>
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Util.ClientErrorTest do
  use ExUnit.Case
  import Polyjuice.Util.ClientError
  import Polyjuice.Util.ClientAPIErrors
  doctest Polyjuice.Util.ClientError

  test "map error with nil defaults" do
    deferror(TestError1)
    test_error = TestError1.new()
    assert test_error.errcode == "TEST_ERROR1"
    assert is_nil(test_error.error)
    assert is_nil(test_error.additional_fields)
  end

  test "map error with macro defaults" do
    deferror(TestError2, "error message", test: "test")
    test_error = TestError2.new()
    assert test_error.errcode == "TEST_ERROR2"
    assert test_error.error == "error message"
    assert test_error.additional_fields == [test: "test"]
  end

  test "map error with error message" do
    deferror(TestError3)
    test_error = TestError3.new("error")
    assert test_error.errcode == "TEST_ERROR3"
    assert test_error.error == "error"
    assert is_nil(test_error.additional_fields)
  end

  test "JSON error encode" do
    deferror(JsonTestError)
    test_error = JsonTestError.new("ERROR", test: "test")
    assert {:ok, encoded} = json_encode(test_error)
    decoded = Jason.decode!(encoded)
    assert %{"errcode" => "JSON_TEST_ERROR", "error" => "ERROR", "test" => "test"} == decoded
  end

  test "JSON encoded MForbidden error" do
    error = Polyjuice.Util.ClientAPIErrors.MForbidden.new()
    assert {:ok, encoded} = json_encode(error)
    decoded = Jason.decode!(encoded)

    assert %{"errcode" => "M_FORBIDDEN", "error" => "You are not allowed to access this"} ==
             decoded
  end
end
